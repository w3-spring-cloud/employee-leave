package com.marlo.w.employeeleave.service;

import com.marlo.w.employeeleave.model.EmployeeLeave;

import java.util.Optional;

public interface EmployeeLeaveService {
    Iterable<EmployeeLeave> getAll();
    EmployeeLeave save(EmployeeLeave employeeLeave);
    Integer deleteById(Integer empId);
    Optional<EmployeeLeave> getById(Integer empId);
}
