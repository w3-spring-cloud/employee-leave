package com.marlo.w.employeeleave.service;

import com.marlo.w.employeeleave.dao.EmployeeLeaveRepository;
import com.marlo.w.employeeleave.model.EmployeeLeave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
public class EmployeeLeaveServiceImpl implements EmployeeLeaveService {

    @Autowired
    private EmployeeLeaveRepository repository;

    @Override
    @Transactional(readOnly = true)
    public Iterable<EmployeeLeave> getAll() {
        return repository.findAll();
    }

    @Override
    public EmployeeLeave save(EmployeeLeave employeeLeave) {
        return repository.save(employeeLeave);
    }

    @Override
    public Integer deleteById(Integer empId) {
        repository.deleteById(empId);
        return 1;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EmployeeLeave> getById(Integer empId) {
        return repository.findById(empId);
    }
}
