package com.marlo.w.employeeleave.model;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table
public class EmployeeLeave {

    @PrimaryKey
    private UUID id;

    private String employeeId;

    private String nickName;

    private String firstName;

    private String lastName;

    @Column("all_leave")
    private Double allLeave;

    @Column("taken_leave")
    private Double takenLeave;

    @Column("remaining_leave")
    private Double remainingLeave;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public Double getAllLeave() {
        return allLeave;
    }

    public void setAllLeave(Double allLeave) {
        this.allLeave = allLeave;
    }

    public Double getTakenLeave() {
        return takenLeave;
    }

    public void setTakenLeave(Double takenLeave) {
        this.takenLeave = takenLeave;
    }

    public Double getRemainingLeave() {
        return remainingLeave;
    }

    public void setRemainingLeave(Double remainingLeave) {
        this.remainingLeave = remainingLeave;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
