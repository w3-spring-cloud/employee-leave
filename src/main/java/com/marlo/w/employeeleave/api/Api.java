package com.marlo.w.employeeleave.api;

import com.marlo.w.employeeleave.model.EmployeeLeave;
import com.marlo.w.employeeleave.service.EmployeeLeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RefreshScope
@RestController
public class Api {

    @Autowired
    private EmployeeLeaveService service;

    @Value("${greeting: Default Hello}")
    private String message;

    @GetMapping("/message")
    public String message() {
        return message;
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<EmployeeLeave> getAll() {
        return service.getAll();
    }

    @RequestMapping(value = "/save", method = {RequestMethod.POST, RequestMethod.PUT})
    public EmployeeLeave insert(@RequestBody EmployeeLeave employeeLeave) {
        return service.save(employeeLeave);
    }

    @RequestMapping(value = "/delete/{empId}", method = RequestMethod.POST)
    public ResponseEntity<EmployeeLeave> delete(@PathVariable Integer empId){
        service.deleteById(empId);

        return  new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{empId}", method = RequestMethod.GET)
    public ResponseEntity<EmployeeLeave> getEmployeeLeave(@PathVariable Integer empId) {
        Optional<EmployeeLeave> leave = service.getById(empId);
        return leave.isPresent()? new ResponseEntity<EmployeeLeave>(leave.get(), HttpStatus.OK): new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
