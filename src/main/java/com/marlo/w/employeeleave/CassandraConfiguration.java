package com.marlo.w.employeeleave;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class CassandraConfiguration extends AbstractCassandraConfiguration {

    @Value("${spring.data.cassandra.keyspace-name}")
    private String keyspaceName;

    @Override
    protected List<String> getStartupScripts() {
        String script = "CREATE KEYSPACE IF NOT EXISTS "+keyspaceName+" WITH replication = { 'replication_factor' : 1, 'class' : 'SimpleStrategy' };";
        String script2 = "CREATE TABLE IF NOT EXISTS "+keyspaceName+".employee (id UUID,employeeId TEXT,nickName TEXT,firstName TEXT,lastName TEXT,all_leave DOUBLE,taken_leave DOUBLE,remaining_leave DOUBLE,primary key((id)));";
        return Arrays.asList(script,script2);
    }

    @Override
    protected List<String> getShutdownScripts() {
        return super.getShutdownScripts();
    }

    @Override
    protected String getKeyspaceName() {
        return keyspaceName;
    }
}
