package com.marlo.w.employeeleave.dao;


import com.marlo.w.employeeleave.model.EmployeeLeave;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeLeaveRepository extends CrudRepository<EmployeeLeave, Integer>
{

}

